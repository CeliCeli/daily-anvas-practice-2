import React, { useEffect, useRef } from "react";

const CANVAS_COLOR = "#232332";
const DOT_COLOR = "#FFFFFF";
const STROKE_COLOR = "#FFFFFF";
const TWO_PI = 2 * Math.PI;
const CIRCLES_COUNT = 3;
const MAX_LENGTH = 800;
const STEP_LENGTH = 2;
const MAX_OFFSET = 8;

function getDistance(a, b) {
  return Math.sqrt(Math.pow(b.x - a.x, 2) + Math.pow(b.y - a.y, 2));
}

export const Canvas = () => {
  const canvasRef = useRef(null);

  useEffect(() => {
    const canvas = canvasRef.current;
    const ctx = canvas.getContext("2d");

    let w = (canvas.width = window.innerWidth);
    let h = (canvas.height = window.innerHeight);

    let mx = 0;
    let my = 0;
    let toggle = 0;

    class Circle {
      constructor(x, y) {
        this.x = x || Math.random() * w;
        this.y = y || Math.random() * h;
      }

      draw(x, y) {
        this.x = x || this.x;
        this.y = y || this.y;
        ctx.lineWidth = 1.5;
        ctx.fillStyle = DOT_COLOR;
        ctx.strokeStyle = STROKE_COLOR;

        ctx.beginPath();
        ctx.arc(this.x, this.y, 6, 0, TWO_PI);
        ctx.closePath();
        ctx.fill();

        ctx.beginPath();
        ctx.arc(this.x, this.y, 32, 0, TWO_PI);
        ctx.closePath();
        ctx.stroke();
      }
    }

    const circles = [];
    for (let i = 0; i < CIRCLES_COUNT; i++) {
      circles.push(new Circle());
    }

    canvas.style.backgroundColor = CANVAS_COLOR;

    function createLightning() {
      for (let a = 0; a < circles.length; a++) {
        for (let b = a + 1; b < circles.length; b++) {
          let dist = getDistance(circles[a], circles[b]);
          let chance = dist / MAX_LENGTH;

          if (chance > Math.random()) continue;
          const otherColor = chance * 255;

          let stepsCount = dist / STEP_LENGTH;
          let sx = circles[a].x;
          let sy = circles[a].y;

          ctx.lineWidth = 2.5;
          ctx.strokeStyle = `rgba(${otherColor},${otherColor},255 )`;
          ctx.beginPath();
          ctx.moveTo(circles[a].x, circles[a].y);
          for (let j = stepsCount; j > 1; j--) {
            let pathLength = getDistance(circles[a], { x: sx, y: sy });
            let offset = Math.sin((pathLength / dist) * Math.PI) * MAX_OFFSET;
            sx += (circles[b].x - sx) / j + Math.random() * offset * 2 - offset;
            sy += (circles[b].y - sy) / j + Math.random() * offset * 2 - offset;
            ctx.lineTo(sx, sy);
          }
          ctx.stroke();
        }
      }
    }

    canvas.onmousemove = (e) => {
      mx = e.x - canvas.getBoundingClientRect().x;
      my = e.y - canvas.getBoundingClientRect().y;
    };

    window.onmousedown = (e) => {
      toggle == circles.length - 1 ? (toggle = 0) : toggle++;
    };

    const loop = () => {
      ctx.clearRect(0, 0, w, h);
      createLightning();
      circles.forEach((circle) =>
        circle == circles[toggle] ? circle.draw(mx, my) : circle.draw()
      );
      requestAnimationFrame(loop);
    };

    loop();

    return () => {
      cancelAnimationFrame(loop);
    };
  }, []);

  return <canvas ref={canvasRef} />;
};
